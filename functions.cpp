/*
 * functions.cpp
 *
 *  Created on: 02-11-2016
 *      Author: Michał Nowaliński
 */
#include "functions.h"
#include "dane_stat.h"

std::vector<std::string> gauss_files;
std::vector<std::string> poisson_files;
std::vector<std::string> lorentz_files;

int getvalue()
{
	char tmp[100];
	bool flag = true;
	do
	{
		std::cin >> tmp;
		char *val = new char[strlen(tmp) + 1];
		strcpy(val, tmp);
		flag = true;
		for(int i = 0; i < (int) strlen(val); i++)
		{
			if(flag == false) continue;
			if(((int) val[i] >= 48) && ((int) val[i] <= 57)) flag = true;
			else flag = false;
		}
		if(flag == false) std::cout << "Wprowadzono zle wartosci, sprobuj jeszcze raz..." << std::endl;
		delete[] val;
	}while(flag == false);
	int value = atoi(tmp);
	return value;
}

std::vector<std::string> get_file_names(std::istream &is)
{
	std::vector<std::string> file_names_vector;
	int len = strlen("http://www.knf.pw.edu.pl/~topie/szpcpp2015-pub/");
	while(is.peek() != EOF)
	{
		std::string string_tmp;
		is >> string_tmp;
		string_tmp.erase(0, len);
		file_names_vector.push_back(string_tmp);
	}
	return file_names_vector;
}

std::string choose_file(std::vector<std::string> &file_names)
{
	bool kontynuuj = true;
	int wybor;
	while(kontynuuj)
	{
		std::cout << "\nWybierz plik, dla którego dopsujemy rozkład!" << std::endl;
		int i = 0;
		for(std::vector<std::string>::const_iterator it = file_names.begin(); it != file_names.end(); it++)
		{
			std::cout << i << ")";
			std::cout << (*it) << std::endl;
			i++;
		}

		wybor = getvalue();
		if(wybor >= 0 && wybor < (int) file_names.size()) return file_names[wybor];

	}

	return "s";
}

unsigned choose_distribution(std::vector<std::string> &file_names, const FabrykaRozkladow &fabryka)
{
	bool kontynuuj = true;
	int wybor;
	MapaNazw m = fabryka.getMapaNazw();

	while(kontynuuj)
	{
		std::cout<<"Wybierz rozkład poprzed wprowadzenie id"<<std::endl;
		for(int i = 1; i <= (int) m.size(); i++)
		{
			std::cout << m[(unsigned) i] <<", id="<<i<< std::endl;
		}

		wybor = getvalue();
		if(wybor >= 1 && wybor <=(int) m.size()) return wybor;


		/*switch (wybor)
		{
			case 1:
			{
				std::cout << "Wybrałeś Gaussa" << std::endl;
				for(std::vector<std::string>::const_iterator it = gauss_files.begin(); it != gauss_files.end(); it++)
				{
					std::cout << (*it) << std::endl;
					dane.push_back(new DaneStatReal(*it));
				}
				//				std::auto_ptr<Rozklad> rozkl(f.utworz(1, dane[0]->dane()));

			}
				break;

			case 2:
			{
				std::cout << "Wybrałeś Lorentza" << std::endl;
				for(std::vector<std::string>::const_iterator it = lorentz_files.begin(); it != lorentz_files.end(); it++)
				{
					std::cout << (*it) << std::endl;
					dane.push_back(new DaneStatReal(*it));
				}
				//				std::auto_ptr<Rozklad> rozkl(f.utworz(1, dane[0]->dane()));

			}
				break;

			case 3:
			{
				std::cout << "Wybrałeś Poissona" << std::endl;
				for(std::vector<std::string>::const_iterator it = poisson_files.begin(); it != poisson_files.end(); it++)
				{
					std::cout << (*it) << std::endl;
					dane.push_back(new DaneStatReal(*it));
				}
				//				std::auto_ptr<Rozklad> rozkl(f.utworz(1, dane[0]->dane()));
			}
				break;
			case 0:
				kontynuuj = false;
				break;
		}*/

	}
}

