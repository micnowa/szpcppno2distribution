/*
 * dane_stat.cpp
 *
 *  Created on: 25-10-2016
 *      Author: Michał Nowaliński
 */
#include "dane_stat.h"
#include <fstream>
#include <iostream>
DaneStatReal::DaneStatReal(const std::string &nazwa) :
	DaneStat(nazwa)
{
	const char *file_name = DaneStat::nazwa_.c_str();
	std::ifstream cin(file_name);
	float tmp;
	while(cin.peek() != EOF)
	{
		cin >> tmp;
		dane_.push_back(tmp);
//		std::cout << tmp << std::endl;
	}
}

const std::string& DaneStat::nazwa() const
{
	std::string str = "asd";
	std::string &ref = str;
	return ref;
}
const std::vector<float>& DaneStatProxy::dane() const
{
	if(!dane_)
	{
		dane_ = new DaneStatReal(nazwa_);
	}
	return dane_->dane();
}

const std::vector<float>& DaneStatReal::dane() const
{
	std::vector<float> &ref = dane_;

	return ref;
}
