/*
 * functions.h
 *
 *  Created on: 02-11-2016
 *      Author: Michał Nowaliński
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include "rozklad.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <string.h>

int getvalue();

std::vector<std::string> get_file_names(std::istream &is);

std::string choose_file(std::vector<std::string> &file_names);

unsigned choose_distribution(std::vector<std::string> &file_names, const FabrykaRozkladow &fabryka);

#endif /* FUNCTIONS_H_ */
