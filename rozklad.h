/*
 * rozklad.h
 *
 *  Created on: 25-10-2016
 *      Author: Michał Nowaliński
 */
#ifndef ROZKLAD_H_
#define ROZKLAD_H_

#include <string>
#include <vector>
#include <map>
#include <memory>

typedef std::map<std::string, float> ParametryRozkladu;// Mapa przechowujaca obliczone estymatory rozkladu: opis-wartosc

class Rozklad
{
public:
	Rozklad(const std::vector<float> &dane) :
		dane_(dane)
	{
	}
	;
	virtual ~Rozklad()
	{
	}
	;
	virtual std::auto_ptr<ParametryRozkladu> oblicz() const = 0;

protected:
	const std::vector<float> &dane_;
};

class RozkladGaussa: public Rozklad
{
public:

	explicit RozkladGaussa(const std::vector<float> &dane) :
		Rozklad(dane)
	{
	}
	;
	virtual ~RozkladGaussa()
	{
	}
	;

	virtual std::auto_ptr<ParametryRozkladu> oblicz() const;

	static Rozklad* kreator(const std::vector<float> &dane);
};

class RozkladLorentza: public Rozklad
{
public:

	explicit RozkladLorentza(const std::vector<float> &dane) :
		Rozklad(dane)
	{
	}
	;
	virtual ~RozkladLorentza()
	{
	}
	;

	virtual std::auto_ptr<ParametryRozkladu> oblicz() const;

	static Rozklad* kreator(const std::vector<float> &dane);
};

class RozkladPoissona: public Rozklad
{
public:

	explicit RozkladPoissona(const std::vector<float> &dane) :
		Rozklad(dane)
	{
	}
	;// nie robi nic sensownego poza wywolaniem konstr. klasy bazowej z odpowiednim parametrem
	virtual ~RozkladPoissona()
	{
	}
	;

	virtual std::auto_ptr<ParametryRozkladu> oblicz() const;// statyczna met. tworzaca i zwracajaca wskaznik na obiekt wlasnego typu

	static Rozklad* kreator(const std::vector<float> &dane);
};

//wskaznik typu KreatorRozkladu do funkcji tworzacej obiekt
//obliczacza rozkladu (pochodna klasy Rozklad)

typedef Rozklad* (*KreatorRozkladu)(const std::vector<float> &);

typedef std::map<unsigned, KreatorRozkladu> MapaKreatorow;
typedef std::map<unsigned, std::string> MapaNazw;


class FabrykaRozkladow // FABRYKA! :)
{
private:
	static MapaKreatorow rozklady_;// przechowuje wskaźniki kreatorow (funkcji tworzących!)
	static MapaNazw nazwy_;// przechowuje nazwy rozkladow

public:
	static void rejestruj(KreatorRozkladu kr, const std::string &nazwa);// rejestruje kreator danego rozkladu (id generowane przyrostowo od 1)

	static Rozklad *utworz(unsigned id, const std::vector<float> &dane);// wola kreator dla rozkladu o wybranym id

	static std::string nazwa(unsigned id);// zwraca nazwe rozkladu o identyfikatorze id

	static unsigned ilosc();// zwraca liczbe zarejestrowanych rozkladow

	MapaNazw getMapaNazw() const;
};

#endif /* ROZKLAD_H_ */
