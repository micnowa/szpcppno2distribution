//============================================================================
// Name        : Distribution.cpp
// Author      : Michał Nowaliński
// Version     :
// Copyright   : Your copyright notice
// Description : Distribution counter
//============================================================================
#include "rozklad.h"
#include "dane_stat.h"
#include "functions.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string.h>
#include <memory>
#include <boost/shared_ptr.hpp>

using namespace std;

int main(int argc, char *argv[])
{
	if(!argc)
	{
		cout << "No text file to read ..." << endl;
		cout << "Program terminated ..." << endl;
		return 0;
	}
	ifstream cin(argv[1]);

	std::vector<std::string> file_names(get_file_names(cin));
	for(std::vector<std::string>::const_iterator it = file_names.begin(); it != file_names.end(); it++)
		cout << *it << endl;

	string file = choose_file(file_names);

	FabrykaRozkladow fabryka;
	fabryka.rejestruj(&RozkladGaussa::kreator, std::string("Rozklad Gaussa"));
	fabryka.rejestruj(&RozkladLorentza::kreator, std::string("Rozklad Lorentza"));
	fabryka.rejestruj(&RozkladPoissona::kreator, std::string("Rozklad Poissona"));

	unsigned distribution_id = choose_distribution(file_names, fabryka);

	MapaNazw mapa = fabryka.getMapaNazw();
	cout << "\nDo pliku: " << file << ", dopasujemy: " << mapa[distribution_id] << endl;

	std::vector<DaneStat*> dane;
	dane.push_back(new DaneStatReal(file));

	std::auto_ptr<Rozklad> rozkl(fabryka.utworz(distribution_id, dane[0]->dane()));
	rozkl->oblicz();

	cout << "Program terminated ...";
	return 0;
}
