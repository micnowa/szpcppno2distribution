/*
 * rozklad.cpp
 *
 *  Created on: 25-10-2016
 *      Author: Michał Nowaliński
 */
#include "rozklad.h"
#include <iostream>
#include <math.h>

std::auto_ptr<ParametryRozkladu> Rozklad::oblicz() const
{
	std::auto_ptr<ParametryRozkladu> ptr(new ParametryRozkladu);
	return ptr;
}

/* GAUSS *********************************************************************** */

Rozklad *RozkladGaussa::kreator(const std::vector<float> & dane)// Statyczny kreator wybranej klasy (to naprawdę jest takie proste!)
{
	return new RozkladGaussa(dane);
}

std::auto_ptr<ParametryRozkladu> RozkladGaussa::oblicz() const
{
	std::auto_ptr<ParametryRozkladu> ptr(new ParametryRozkladu);

	float odchylenie = 0, srednia = 0;
	for(std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		srednia += *it;
		std::cout << srednia / dane_.size() << std::endl;
	}
	srednia /= dane_.size();

	for(std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		odchylenie += pow((*it) - srednia, 2);
		std::cout << odchylenie / dane_.size() << std::endl;
	}
	odchylenie /= (dane_.size() - 1);
	odchylenie = sqrt(odchylenie);

	(*ptr)["srednia"] = srednia;
	(*ptr)["odchylenie"] = odchylenie;

	std::cout << (*ptr)["srednia"] << "    " << (*ptr)["odchylenie"] << std::endl;

	return ptr;
}
/* *********************************************************************************************************************************** */

/* LORENTZ *********************************************************************** */

Rozklad *RozkladLorentza::kreator(const std::vector<float> & dane)// Statyczny kreator wybranej klasy (to naprawdę jest takie proste!)
{
	return new RozkladLorentza(dane);
}

std::auto_ptr<ParametryRozkladu> RozkladLorentza::oblicz() const
{
	std::auto_ptr<ParametryRozkladu> ptr(new ParametryRozkladu);

	float odchylenie = 0, srednia = 0;
	for(std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		srednia += *it;
		std::cout << srednia / dane_.size() << std::endl;
	}
	srednia /= dane_.size();

	for(std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		odchylenie += pow((*it) - srednia, 2);
		std::cout << odchylenie / dane_.size() << std::endl;
	}
	odchylenie /= (dane_.size() - 1);
	odchylenie = sqrt(odchylenie);

	(*ptr)["srednia"] = srednia;
	(*ptr)["odchylenie"] = odchylenie;

	std::cout << (*ptr)["srednia"] << "    " << (*ptr)["odchylenie"] << std::endl;

	return ptr;
}
/* *********************************************************************************************************************************** */
//1004.69    15.2439
/* POISSON *********************************************************************** */

Rozklad *RozkladPoissona::kreator(const std::vector<float> & dane)// Statyczny kreator wybranej klasy (to naprawdę jest takie proste!)
{
	return new RozkladPoissona(dane);
}

std::auto_ptr<ParametryRozkladu> RozkladPoissona::oblicz() const
{
	std::auto_ptr<ParametryRozkladu> ptr(new ParametryRozkladu);
	float srednia = 0;
	for(std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		srednia += *it;
		std::cout << srednia / dane_.size() << std::endl;
	}

	srednia /= dane_.size();

	(*ptr)["srednia"] = srednia;
	(*ptr)["odchylenie"] = sqrt(srednia);

	std::cout << (*ptr)["srednia"] << "    " << (*ptr)["odchylenie"] << std::endl;

	return ptr;
}
/* *********************************************************************************************************************************** */

/* FABRYKA *********************************************************************** */
MapaKreatorow init_MapaKreatorow()
{
	MapaKreatorow *some_map = new MapaKreatorow();
	return *some_map;
}

MapaNazw init_MapaNazw()
{
	MapaNazw *some_map2 = new MapaNazw();
	return *some_map2;
}

MapaKreatorow FabrykaRozkladow::rozklady_ = init_MapaKreatorow();

MapaNazw FabrykaRozkladow::nazwy_ = init_MapaNazw();

Rozklad* FabrykaRozkladow::utworz(unsigned id, const std::vector<float> & dane)// Statyczna metoda wołająca (choć trudno to zobaczyć) kreator typu o podanym id
{
	return rozklady_[id](dane);
}

void FabrykaRozkladow::rejestruj(KreatorRozkladu kr, const std::string &nazwa)
{
	int id = nazwy_.size() + 1;
	nazwy_[id] = nazwa;
	rozklady_[id] = kr;

	std::cout << "Rejestruję: " << nazwa << ",przypisuję id=" << id << std::endl;
}

std::string FabrykaRozkladow::nazwa(unsigned id)
{
	return nazwy_[id];
}

unsigned FabrykaRozkladow::ilosc()
{
	return rozklady_.size();
}

MapaNazw FabrykaRozkladow::getMapaNazw() const
{
	return nazwy_;
}
