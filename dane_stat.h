/*
 * dane_stat.h
 *
 *  Created on: 25-10-2016
 *  Author: Michał Nowaliński
 */
#ifndef DANE_STAT_H_
#define DANE_STAT_H_
#include <iostream>
#include <string>
#include <vector>

class DaneStat// klasa bazowa
{
public:
	DaneStat(const std::string &nazwa)
	{
		nazwa_ = nazwa;
	}
	virtual ~DaneStat()
	{

	}

	virtual const std::vector<float> &dane() const =0; // zwraca referencje na przechowywane dane
	virtual const std::string &nazwa() const;// zwraca nazwe pliku

protected:
	mutable std::vector<float> dane_;
	std::string nazwa_;// nazwa pliku
};

class DaneStatProxy: public DaneStat// proxy - wczytuje prawdziwy obiekt przy pierwszym użyciu
{
private:
	mutable DaneStat *dane_;
public:
	DaneStatProxy(const std::string &nazwa) : DaneStat(nazwa) , dane_(NULL) {};
	~DaneStatProxy(){ delete dane_; };
	virtual const std::vector<float> &dane() const;
};


class DaneStatReal: public DaneStat// proxy - wczytuje prawdziwy obiekt przy pierwszym użyciu
{
public:
	DaneStatReal(const std::string &nazwa);
	~DaneStatReal()
	{

	}

	virtual const std::vector<float> &dane() const;
};

#endif /* DANE_STAT_H_ */
